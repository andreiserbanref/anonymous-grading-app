# Anonymous Grading App
Andrei Serban, Eduard Stan - AS Students

## Objective
Developing a web application which allows student projects to be graded by anonymous juries of coleagues.
## Description
The application should allow students projects to be graded by anonymous juries of peers.

The application is built on a Single Page Application architecture and is accessible from the browser on the desktop, mobile devices or tablets (depending on user preference).
## (Minimal) functionality
- As a student member in a project team (PM) i can add a project and define a series of partial project deliverables. By registering i automatically become eligible as an evaluator.
- As a PM i can add a demonstrative video or a link to a server hosting the deployed project for any partial deliverable.
- When a partial deliverable is due, any student who is not a PM for a particular project can be randomly selected to be part of the project jury. A student which is a member of said jury can grade the project. 
- The grade for the project is anonymous and the total grade is calculated by omitting the lowest and highest grades and then averaging the remaining ones. The grades are 1-10 with at most 2 fractional digits.
- As a professor, i can see the results of the evaluation for each project, without being able to see the identity of the jury members.
- The application has a sistem of permissions. Only a member of the jury can add/modify grades and they can only modify their own grades. Grades can only be modified for a limited period of time.

---
## User Stories

### Student
- As a student, I want to register on the app;
- As a student, I can create a project;
- As a student, I can add other students as team members on my project;
- As a student, I have the option to be Team Member for specific projects or Evaluator for random projects which I am not assigned to;

### MP (student member in a project team)
- As a PM, I can see all the projects I am assigned to;
- As a PM, I can see a project page (if I am assigned to that specific project);
- As a PM, I can define a series of partial project deliverables;
- As a PM, I can add a demonstrative video or a link to a server hosting the deployed project for any partial deliverable;
- As a PM, I can't evaluate my own project;
- As a PM, I can see the final grade of my project;

### Evaluator/Jury Member 
- As an Evaluator, I can see all the projects where I'm assigned as an Evaluator;
- As an Evaluator, I can see the project page (if I am assigned to that specific project);
- As an Evaluator, I can grade the project;

### Professor
- As a Professor, I can see all the projects;
- As a Professor, I can see all the projects' specifications;
- As a Professor, I can see the results of the evaluation for each project, without being able to see the identity of the jury members;

--- 
## Technologies Stack
- Javascript (Node.js)
- 







